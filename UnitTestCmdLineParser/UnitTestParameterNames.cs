﻿using System;
using System.Linq;
using CommandLineParser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestCmdLineParser
{
    [TestClass]
    public class UnitTestParameterNames
    {
        private static string _fullNameParameter1Value = String.Empty;
        private static string _fullNameParameter2Value = String.Empty;
        private static string _shortNameParameter3Value = String.Empty;

        [ClassInitialize()]
        public static void Init(TestContext context)
        {
            string[] args = { "FullNameParameter1=123", "short2=456", "FullNameParameter3=789" };

            args.Process(
              () => Console.WriteLine("Test method has been ran with wrong parameters"),
              new CommandLine.Switch("FullNameParameter1",
                  val => _fullNameParameter1Value = string.Join(" ", val)),
              new CommandLine.Switch("FullNameParameter2",
                  val => _fullNameParameter2Value = string.Join(" ", val), "short2"),
              new CommandLine.Switch("FullNameParameter3",
                  val => _shortNameParameter3Value = string.Join(" ", val), "short3"));
        }

        [TestMethod]
        public void TestFullNameParameter1Handling()
        {
            string expected = "123";
            Assert.AreEqual(expected, _fullNameParameter1Value);
        }

        [TestMethod]
        public void TestFullNameParameter2Handling()
        {
            string expected = "456";
            Assert.AreEqual(expected, _fullNameParameter2Value);
        }

        [TestMethod]
        public void TestShortNameParameter3Handling()
        {
            string expected = "789";
            Assert.AreEqual(expected, _shortNameParameter3Value);
        }
    }
}
