﻿using System;
using System.Linq;
using CommandLineParser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
namespace UnitTestCmdLineParser
{
    [TestClass]
    public class UnitTestParameterValues
    {
        private static Dictionary<string, string> _values = new Dictionary<string, string>();

        [ClassInitialize()]
        public static void Init(TestContext context)
        {
            string[] args = { "p1=123", "p2=12.34", "p3=alice_has_a_cat", "p4=01/10/2013", "p5=\"C:\\sample location\\of my file.txt\"" };

            args.Process(
              () => Console.WriteLine("Test method has been ran with wrong parameters"),
              new CommandLine.Switch("p1",
                  val => _values.Add("p1",string.Join(" ", val))),
              new CommandLine.Switch("p2",
                  val => _values.Add("p2", string.Join(" ", val))),
              new CommandLine.Switch("p3",
                  val => _values.Add("p3", string.Join(" ", val))),
              new CommandLine.Switch("p4",
                  val => _values.Add("p4", string.Join(" ", val))),
              new CommandLine.Switch("p5",
                  val => _values.Add("p5",string.Join(" ", val))));
        }

        [TestMethod]
        public void TestIntegerValue()
        {
            string expected = "123";
            string currentValue = String.Empty;
            _values.TryGetValue("p1", out currentValue);
            Assert.AreEqual(expected, currentValue);
        }

        [TestMethod]
        public void TestDecimalValue()
        {
            string expected = "12.34";
            string currentValue = String.Empty;
            _values.TryGetValue("p2", out currentValue);
            Assert.AreEqual(expected, currentValue);
        }

        [TestMethod]
        public void TestStringValue()
        {
            string expected = "alice_has_a_cat";
            string currentValue = String.Empty;
            _values.TryGetValue("p3", out currentValue);
            Assert.AreEqual(expected, currentValue);
        }

        [TestMethod]
        public void TestDateValue()
        {
            string expected = "01/10/2013";
            string currentValue = String.Empty;
            _values.TryGetValue("p4", out currentValue);
            Assert.AreEqual(expected, currentValue);
        }

        [TestMethod]
        public void TestFilePathValue()
        {
            string expected = "C:\\sample location\\of my file.txt";
            string currentValue = String.Empty;
            _values.TryGetValue("p5", out currentValue);
            Assert.AreEqual(expected, currentValue);
        }
    }
}
