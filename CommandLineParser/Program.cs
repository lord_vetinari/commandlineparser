﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandLineParser
{
    class Program
    {
        // This is DEMO program
        // We expect here parameters as fallows:
        // inputFiles="C:\input\file_001.txt","C:\input\file_002.txt","C:\input\file_003.txt" configFile="C:\tests\configuration.xml"
        // or
        // inputFiles="C:\input\file_001.txt","C:\input\file_002.txt","C:\input\file_003.txt" cfg="C:\tests\configuration.xml"
        static void Main(string[] args)
        {
            args.Process(
               () => Console.WriteLine("Usage is inputFiles=value1,value2 configFile=value3"),
               new CommandLine.Switch("inputFiles",
                   val => Console.WriteLine("inputFiles with value {0}",
                       string.Join(" ", val))),
               new CommandLine.Switch("configFile",
                   val => Console.WriteLine("configFile with value {0}",
                       string.Join(" ", val)), "cfg"));

            Console.ReadKey();
       }
    }
}
